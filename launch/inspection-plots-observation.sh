#!/bin/bash
OBSID=$1

exec sbatch \
    --partition=cpu \
    --nice=500 --time=1-0 \
    --qos=inspectionplots \
    --ntasks=49 --cpus-per-task=2 \
    --ntasks-per-node=7 \
    --job-name="inspection-$OBSID" \
    --output=/data/log/inspection-plots-$OBSID.log \
    --export=TAG=latest \
    docker-launch-msplots.sh "$@"
